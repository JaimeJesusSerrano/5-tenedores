import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Button, Icon} from 'react-native-elements'

export default function App() {
  return (
    <View style={styles.container}>
      <Text>5 Tenedores</Text>
      <Button
        icon={
          <Icon
            color="white"
            name="check-outline"
            type="material-community"
            size={15}
          />
        }
        title="Click me"
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
